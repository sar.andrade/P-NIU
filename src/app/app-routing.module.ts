import { InicioComponent } from './views/Inicio/Inicio.component';
import { ApiComponent } from './views/Api/Api.component';
import { DesarrolladoresComponent } from './views/desarrolladores/desarrolladores.component';
import { PresentacionComponent } from './views/Presentacion/Presentacion.component';
import { InicioSComponent } from './views/InicioS/InicioS.component';
import {DocumentacionComponent} from './views/Documentacion/Documentacion.component'
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'InicioS', component: InicioSComponent},
  {path: 'Presentacion', component: PresentacionComponent},
  {path: 'desarrolladores', component: DesarrolladoresComponent},
  {path: 'Documentacion', component: DocumentacionComponent},
  {path: 'Inicio', component: InicioComponent},
  {path: '', redirectTo: '/Presentacion', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
