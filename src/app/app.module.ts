import { FooterComponent } from './views/Footer/Footer.component';
import { VentasComponent } from './views/Ventas/Ventas.component';
import { NavbarComponent } from './views/navbar/navbar.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {PresentacionComponent} from './views/Presentacion/Presentacion.component';
import {VideoIntroComponent} from './views/Video-Intro/Video-Intro.component';
import {BtnIniciarComponent} from './views/Btn-Iniciar/Btn-Iniciar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {Cuerpo2Component} from './views/Cuerpo-2/Cuerpo-2.component'
import {SlideComponent} from './views/Slide/Slide.component'
import {BtnInicioSesionComponent} from './views/Btn-Inicio-Sesion/Btn-Inicio-Sesion.component'
import { ErrorTailorModule } from '@ngneat/error-tailor';
import {PlayStoreComponent} from './views/PlayStore/PlayStore.component'
import {AppStoreComponent} from './views/AppStore/AppStore.component'
import {Cuerpo3Component} from './views/Cuerpo3/Cuerpo3.component'
import {Btn_DocumentacionComponent} from './views/Btn_Documentacion/Btn_Documentacion.component'
import {Cuerpo4Component} from './views/Cuerpo4/Cuerpo4.component'
import {DocumentacionComponent} from './views/Documentacion/Documentacion.component'
import {ApiComponent} from './views/Api/Api.component'
import {InicioComponent} from './views/Inicio/Inicio.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PresentacionComponent,
    VentasComponent,
    VideoIntroComponent,
    BtnIniciarComponent,
    Cuerpo2Component,
    SlideComponent,
    FooterComponent,
    BtnInicioSesionComponent,
    PlayStoreComponent,
    AppStoreComponent,
    Cuerpo3Component,
    Btn_DocumentacionComponent,
    Cuerpo4Component,
    DocumentacionComponent,
    ApiComponent,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ErrorTailorModule.forRoot({
      errors: {
        useValue: {
          required: 'This field is required',
          minlength: ({ requiredLength, actualLength }) =>
                      `Expect ${requiredLength} but got ${actualLength}`,
          invalidAddress: error => `Address isn't valid`
        }
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
