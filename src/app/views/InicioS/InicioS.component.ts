import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-InicioS',
  templateUrl: './InicioS.component.html',
  styleUrls: ['./InicioS.component.css']
})
export class InicioSComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  registroPrincipal(){
    this.router.navigate(['/Presentacion'])
  }
}
