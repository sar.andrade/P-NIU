import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-Btn_Documentacion',
  templateUrl: './Btn_Documentacion.component.html',
  styleUrls: ['./Btn_Documentacion.component.css']
})
export class Btn_DocumentacionComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  Documentacion(){
    this.router.navigate(['/Documentacion'])
  }
}
