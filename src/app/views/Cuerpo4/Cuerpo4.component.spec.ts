/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Cuerpo4Component } from './Cuerpo4.component';

describe('Cuerpo4Component', () => {
  let component: Cuerpo4Component;
  let fixture: ComponentFixture<Cuerpo4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cuerpo4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cuerpo4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
