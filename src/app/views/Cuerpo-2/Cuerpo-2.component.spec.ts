/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Cuerpo-2Component } from './Cuerpo-2.component';

describe('Cuerpo-2Component', () => {
  let component: Cuerpo-2Component;
  let fixture: ComponentFixture<Cuerpo-2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cuerpo-2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cuerpo-2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
