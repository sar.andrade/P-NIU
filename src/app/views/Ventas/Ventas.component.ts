import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-Ventas',
  templateUrl: './Ventas.component.html',
  styleUrls: ['./Ventas.component.css']
})
export class VentasComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
  }

  empezar(){
    this.router.navigate(['/InicioS'])
  }
}
