import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-Btn-Iniciar',
  templateUrl: './Btn-Iniciar.component.html',
  styleUrls: ['./Btn-Iniciar.component.css']
})
export class BtnIniciarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  sesion(){
    this.router.navigate(['/InicioS'])
  }

}
